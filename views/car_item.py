from PySide import QtGui, QtCore


class CarItem(QtGui.QGraphicsItem):

    rect = QtCore.QRectF(0, 0, 20, 20)

    def boundingRect(self):
        return CarItem.rect

    def paint(self, painter, options, widget):
        painter.save()
        painter.setPen(QtCore.Qt.NoPen)
        color = QtCore.Qt.black
        brush = QtGui.QBrush(color)
        painter.setBrush(brush)
        painter.drawEllipse(CarItem.rect)
        painter.restore()