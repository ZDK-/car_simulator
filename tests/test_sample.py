def test_creation():
    from models.car import Car
    car = Car(position=(5, 10), speed=10.0, angle=0)
    assert car.x == 5
    assert car.y == 10
    assert car.speed == 10


def test_get_position():
    from models.car import Car
    car = Car(position=(10, 15), speed=10.0, angle=0)
    assert car.get_position() == (10, 15)


def test_update_position():
    from models.car import Car
    import math
    car = Car(position=(0, 0), speed=10.0, angle=30)
    car.x = 100
    car.y = 5
    assert car.x == 100
    assert car.y == 5
    car.update_position()
    assert car.x == 100 + math.cos(math.radians(30)) * 10.0
    assert car.y == 5 + math.sin(math.radians(30)) * 10.0


def test_increase_speed():
    from models.car import Car
    import math
    car = Car(position=(0, 0), speed=0.0, angle=30)
    car.speed += 10
    car.update_position()
    assert car.x == math.cos(math.radians(30)) * 10.0
    assert car.y == math.sin(math.radians(30)) * 10.0


def test_decrease_speed():
    from models.car import Car
    import math
    car = Car(position=(0, 0), speed=20.0, angle=30)
    car.speed -= 10
    car.update_position()
    assert car.x == math.cos(math.radians(30)) * 10.0
    assert car.y == math.sin(math.radians(30)) * 10.0


def test_get_speed_in_kph():
    from models.car import Car
    car = Car(position=(0, 0), speed=10.0, angle=0)

    assert car.speed_in_kph == 36.0

    car.speed = 25.0
    assert car.speed_in_kph == 90.0


def test_minimum_speed():
    from models.car import Car
    car = Car(position=(0, 0), speed=10.0, angle=30)
    car.speed -= 100
    assert car.speed >= 0
