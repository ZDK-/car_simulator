import sys
from functools import partial

from PySide import QtGui, QtCore

from models.car import Car
from views.car_item import CarItem

items = {}


def update():
    for item, car in items.iteritems():
        car.update_position()
        item.setPos(*car.get_position())


def main():
    app = QtGui.QApplication(sys.argv)
    QtCore.qsrand(QtCore.QTime(0, 0, 0).secsTo(QtCore.QTime.currentTime()))

    scene = QtGui.QGraphicsScene()
    scene.setSceneRect(-300, -300, 600, 600)
    scene.setItemIndexMethod(QtGui.QGraphicsScene.NoIndex)

    car_item = CarItem()
    scene.addItem(car_item)

    car = Car((0, 0), 1, 30)
    items[car_item] = car

    view = QtGui.QGraphicsView(scene)
    view.setRenderHint(QtGui.QPainter.Antialiasing)
    view.setWindowTitle('Car Simulator')
    view.resize(500, 300)

    window = QtGui.QMainWindow()
    central_widget = QtGui.QWidget(window)
    window.setCentralWidget(central_widget)
    layout = QtGui.QVBoxLayout(window)
    central_widget.setLayout(layout)
    layout.addWidget(view)

    speedup_btn = QtGui.QPushButton("Speed Up", window)
    layout.addWidget(speedup_btn)
    speedup_btn.released.connect(partial(increase_speed_callback, car))

    slowndown_btn = QtGui.QPushButton("Slow Down", window)
    layout.addWidget(slowndown_btn)
    slowndown_btn.released.connect(partial(decrease_speed_callback, car))

    speedometer = QtGui.QLabel("Speed", window)
    layout.addWidget(speedometer)

    window.show()

    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(40)

    sys.exit(app.exec_())


def increase_speed_callback(car):
    car.speed += 1


def decrease_speed_callback(car):
    car.speed -= 1


if __name__ == '__main__':
    main()
