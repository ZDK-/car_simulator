from PySide.QtCore import QObject
import math


class Car(QObject):
    def __init__(self, position, speed, angle):
        self.x, self.y = position
        self.angle = math.radians(angle)
        self._speed = speed

    def get_position(self):
        return (self.x, self.y)

    def update_position(self):
        self.angle_vector = Vector(math.cos(self.angle), math.sin(self.angle))
        self.x += self.angle_vector.x * self.speed
        self.y += self.angle_vector.y * self.speed

    @property
    def speed_in_kph(self):
        return self.speed * 3.6

    def __repr__(self):
        return 'position: ({x}, {y}), speed: {speed}, angle: {degree}'.format(
            x=self.x,
            y=self.y,
            speed=self.speed,
            degree=math.degrees(self.angle)
        )
    @property
    def speed(self):
        return self._speed

    @speed.setter
    def speed(self, value):
        self._speed = value
        if self._speed < 0:
            self._speed = 0


class Vector():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def length(self):
        return math.hypot(self.y, self.x)

    def __repr__(self):
        return 'vector: ({x}, {y})'.format(
            x=self.x,
            y=self.y
        )

def normalize(vector):
    v_length = vector.length()
    return Vector(vector.x / v_length, vector.y / v_length)




